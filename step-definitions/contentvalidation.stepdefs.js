const { Given, When, Then } = require('cucumber');
let { getWebdriver } = require('../utils/browser');
const { getConfig } = require('../utils/utils.js');
const { getMarket } = require('../utils/utils.js');
const { Builder, By, Key, until } = require('selenium-webdriver');
let { getEnv } = require('../utils/utils.js');
var expect = require('chai').expect;
let Page = require('../pageObjects/base.page');


When(/^user navigates to \"([^\"]*)\" application in \"([^\"]*)\"$/, async function (product, market) {
    console.log(`User Navigates to URL`);
    let page = new Page(this.client);
    let url = getConfig().config[getEnv()].domain + getConfig().products[product].uri;
    await page.navigateTo(url);
    await this.client.sleep(10000);
});

//user validates the content of the below fields in "aboutyou" section
Then(/^user validates the content of the below fields in \"([^\"]*)\" section in \"([^\"]*)\" page for \"([^\"]*)\"$/, async function (section, pageObj, product, dataTable) {

    let page = new Page(this.client);
    let rows = dataTable.raw();
    let content = require(`../content/${getMarket()}/${product}.content`);
    let selector = require(`../config/${getMarket()}/selectors`);
    if (content.hasOwnProperty(section)) {
        let validation = content[section];
        for (let i = 0; i < rows.length; i++) {
            console.log(`<---------Rows inside the loop:----->  ${rows[i][0]}`);
            if (typeof validation[rows[i][0]] === 'string') {
                let actual = await page.getText(selector['selectors'][section][rows[i][0]].input);
                let expected = validation[rows[i][0]];
                console.log(`Actual: -----> ${actual}`);
                console.log(`Expected:-----> ${expected}`);
                expect(actual).to.equal(expected);
            } else if (typeof validation[rows[i][0]] === 'object') {
                for (let [key, value] of Object.entries(validation[rows[i][0]])) {
                    let container = selector['selectors'][section][rows[i][0]].input;
                    let sel = `(${container})[${key}]`;
                    let act = await page.getText(sel);
                    console.log(`Actual:-----> ${act}`);
                    console.log(`Expected:----->${value}`);
                    expect(act).to.equal(value);
                }
            }
            console.log(`<------End of Loop------->`);
        }

    }

});

Then(/^user validates the links of the below fields in \"([^\"]*)\" section in \"([^\"]*)\" page for \"([^\"]*)\"$/, async function (section, pageObj, product, dataTable) {
   
    let page = new Page(this.client);
    let rows = dataTable.raw();
    let content = require(`../content/${getMarket()}/${product}.content`);
    let selector = require(`../config/${getMarket()}/selectors`);
    if (content.hasOwnProperty(section)) {
        let validation = content[section];
        for (let i = 0; i < rows.length; i++) {
            console.log(`<---------Rows inside the loop:----->  ${rows[i][0]}`);
            if (typeof validation[rows[i][0]] === 'string') {
                let actual = await page.getUrlSrc(selector['selectors'][section][rows[i][0]].input);
                let expected = validation[rows[i][0]];
                console.log(`Actual: -----> ${actual}`);
                console.log(`Expected:-----> ${expected}`);
                expect(actual).to.equal(expected);
            } else if (typeof validation[rows[i][0]] === 'object') {
                for (let [key, value] of Object.entries(validation[rows[i][0]])) {
                    let container = selector['selectors'][section][rows[i][0]].input;
                    let sel = `(${container})[${key}]`;
                    let act = await page.getUrlSrc(sel);
                    console.log(`Actual:-----> ${act}`);
                    console.log(`Expected:----->${value}`);
                    expect(act).to.equal(value);
                }
            }
            console.log(`<------End of Loop------->`);
        }

    }
});

When(/^I Login with the default user$/, async function () {
    console.log('Logging the Page Title:    ', await this.client.getTitle());
});

Then(/^I should see the flight finder Page$/, function () {
    console.log('Just Logging');
});