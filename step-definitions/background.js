/**
 * This file contains all the background steps that are necessary before running any tests
 */

const {Given,When,Then,setDefaultTimeout,After,Before,AfterAll} = require ('cucumber');
let {getWebdriver} = require('../utils/browser');
const {getConfig} = require('../utils/utils.js');
let {getEnv} = require('../utils/utils.js');
let {By,until} = require('selenium-webdriver');
let Page = require('../pageObjects/base.page');
let page;
let scenario1;
let world;

setDefaultTimeout(80*1000);

// The below Method instantiates the browser and assigns the execution context using 'this'
Given(/^The environment and browser are ready$/,async function(){

    //get the client here from base page
    let client = getWebdriver().build();

    //sets global context to client by binding it to this.client
    this.client = client;
    console.log('Browser has been initialized!');
});


// The above methods were designed based on the instructions from npm package of cucumber-html-report
Before(function(scenario){
    scenario1 = scenario;
    world =this;
    global.scenario = scenario1;
});

After(async function(){
    //'#rightWrapperImg'
    //await this.client.wait(until.elementIsVisible(await this.client.findElement(By.css('#rightWrapperImg'))),1000);
    await this.client.sleep(1000);
    await this.client.takeScreenshot().then(function (buffer) {
        return world.attach(buffer, 'image/png');
});
   // await this.client.quit();
});

