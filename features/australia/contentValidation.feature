@basic
Feature: To Validate the page contents of the application

    Background: Common Step
        Given The environment and browser are ready

    @basic
    Scenario Outline: Login Test
        When user navigates to "<product>" application in "australia"
        Then user validates the content of the below fields in "Header" section in "aboutyou" page for "<product>"
            | cardname |
        Then user validates the links of the below fields in "Header" section in "aboutyou" page for "<product>"
            | cardart |
        Then user validates the content of the below fields in "Sidebar" section in "aboutyou" page for "<product>"
            | cardname |
            | phone    |
        Then user validates the links of the below fields in "Sidebar" section in "aboutyou" page for "<product>"
            | cardart |
        Then user validates the content of the below fields in "aboutyou" section in "aboutyou" page for "<product>"
            | topsectionheader    |
            | topsectionpara      |
            | bottomsectionheader |
            | bottomsectionli     |


        Examples: iterations
            | product         |
            | platinum_charge |
            | tbultimate      |