const {getSelectors} = require('../utils/utils.js');
const webdriver = require('selenium-webdriver');
const {Builder, By, Key, until,wait} = require('selenium-webdriver');
const {getBrowser} = require('../utils/utils.js');

let driver;

class Page{

    constructor(client){
        this.client = client;
        //this.selectors = getSelectors();
    }

   async navigateTo(url){
       return await this.client.get(url);
    }


    /**
     * get Text from an element
     * @param selector
     */
     async getText(selector) {
         return await this.client.findElement(By.xpath(selector)).getText();            
    }

    async getUrlSrc(selector){
        return await this.client.findElement(By.xpath(selector)).getAttribute('src');
    }

    /**
     * To set Text in a field
     * @param selector
     * @param text
     */
    setText(selector, text) {
        return this.client
            .waitForVisible(selector,10000)
            .setValue(selector, text);
    }

    sendKeys(selector, text) {
        return await this.client.findElement(By.xpath(selector)).sendKeys(text);
    }

    /**
     * To Click
     * @param selector
     */
    async click(selector) {
        return await this.client.findElement(By.xpath(selector)).click();
    }

    
}

module.exports = Page;