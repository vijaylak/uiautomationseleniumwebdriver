const selectors = {
    Header: {
        cardart: {
            input: '//*[@id="card-art-wrapper"]/img'
        },
        cardname: {
            input: '//*[@id="productName"]'
        }
    },
    Sidebar: {
        cardart: {
            input: '//*[@id="rightWrapperImg"]'
        },
        cardname: {
            input: '//*[@id="rightWrapper"]/div[2]/div[1]/p'
        },
        phone: {
            input: '//a[@class="phone"]'
        },
        faq: {
            input: '//a[@class="faq"]'
        }
    },
    aboutyou: {
        topsectionheader:{
            input:'//*[@class = "headerSection"]//h3'
        },
        topsectionpara:{
            input:'//*[@class = "headerSection"]//p'
        },
        bottomsectionheader:{
            input:'//*[contains(@class,"declaration stage1")]//div//div[contains(@class,"section-field-label maxWidth")]'
        },
        bottomsectionli:{
            input:'//*[contains(@class,"declaration stage1")]//li'
        },
        title:{
            input:'(//select[contains(@class,"form-control")])[1]'
        },
        dob_d:{
            input:'(//select[contains(@class,"form-control")])[2]'
        },
        dob_m:{
            input:'(//select[contains(@class,"form-control")])[3]'
        },
        dob_y:{
            input:'(//select[contains(@class,"form-control")])[4]'
        },
        add_y:{
            input:'(//select[contains(@class,"form-control")])[5]'
        },
        add_m:{
            input:'(//select[contains(@class,"form-control")])[5]'
        },
        firstname:{
            input:'//*[@id="AU_F2"]'
        },
        lastname:{
            input:'//*[@id="AU_F4"]'
        },
        email:{
            input:'//*[@id="AU_F5"]'
        },
        mobile:{
            input:'//*[@id="AU_F4"]'
        },
        address:{
            input:'//*[@id="AU_F11"]'
        },
        lielement:{
            input:'//*[contains(@class,"qasOptions")]//li[1]'
        },
        annualSal:{
            input:'//*[@id="AU_F18"]'
        }



    }
};

module.exports = {
    selectors
}