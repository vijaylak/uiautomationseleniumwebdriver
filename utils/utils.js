const _ = require('lodash');
const {get} = require('lodash');
const yargs = require('yargs');

// Below method is to parse the cmd line args

const parseCmdArgs = () => {
    return yargs.argv;
};

const getCmdArgs = parseCmdArgs();

/**
 * The Below methods are used to parse specific cmd line values based on their Key
 * The Key is mentioned beside the argument
 * Since this Automation focuses on the Australia Market with a variety 
 */

 //get value of Key 'market' from the cmd line
 const getMarket = () => parseCmdArgs()['market'];

 //get value of Key 'product' from the cmd line
 const getProduct = () => parseCmdArgs()['product'];

 //get value of Key 'env ( test environment)' from the cmd line. This can be changed as per the changes in the test env
 let getEnv = () => parseCmdArgs()['env'];

 //get value of Key 'browser Type' from the cmd line
 const getBrowser = () => parseCmdArgs()['browser'];

  //get value of Key 'OS Type' from the cmd line
 const getOS = () => parseCmdArgs()['os'];

 //The below method returns the market specific configuration
 const getConfig = () => {
    const market = getMarket();
    const product = getProduct();
    let env = getEnv();
    const config = require('../config/'+market+'/config');

    //const _config = _.merge({},config);

    return config;
 };

 
 //the below method returns market specific selectors
 const getSelector = () => {
    const market = getMarket();
    let selectors = require('../config'+market+'/selectors');
 };

 module.exports = {
    getConfig,getMarket,getProduct,getOS,getEnv,getSelector,getBrowser,getCmdArgs
 };



