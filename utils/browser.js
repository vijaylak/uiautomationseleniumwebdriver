const webdriver = require('selenium-webdriver');
const {Builder, By, Key, until,Capabilities} = require('selenium-webdriver');
const {getBrowser} = require('./utils');
/*const browser = new webdriver.Builder()
  .usingServer()
  .withCapabilities({'browserName': 'chrome' })
  .build();*/

  //Returns the driver based on the browser parameter from the cmd line args

const capabilities = new Capabilities();
capabilities.setBrowserName(getBrowser());
capabilities.setPageLoadStrategy("normal");

const getWebdriver = () =>{
   let driver = new webdriver.Builder()
                .usingServer()
                .withCapabilities(capabilities);
                
  return  driver;
};

module.exports = {
    getWebdriver
};