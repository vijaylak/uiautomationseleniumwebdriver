/*
The below helps in generating a html report using the 'cucumber-html-reporter' package
The below code was used from: 
https://www.npmjs.com/package/cucumber-html-reporter
*/

let reporter = require('cucumber-html-reporter');
 
let options = {
        theme: 'bootstrap',
        jsonFile: './report/cucumber_report.json',
        output: './report/cucumber_report.html',
        reportSuiteAsScenarios: true,
        scenarioTimestamp: true,
        screenshotsDirectory: './screenshots/',
        storeScreenshots: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "STAGING",
            "Browser": "Chrome",
            "Platform": "Mac Os",
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    };
 
reporter.generate(options);