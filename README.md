# Project Title: UI Automation using SeleniumWebdriver	
This is a highly felxible and Scalable Javascript based UI Automation framework to test front end applications.
The Framework uses Cucumber to write test cases, selenium-webdriver to interact with the browser and chai for assertions.
Users have the flexibility to use either the wdio test runner using the wdio.conf.js or cucumber-js runner by configuring it in the package.json scripts.
The sample test cases written primarily focusses testing the americanexpress Australia application that is available over the internet.
This Framework was built with flexibility in mind and can be used to onboard any market like India,Canada,etc as the page template for all these applications are the same. 

#### This sample does not contain the basic selenium interactions like form fill/click etc
#### it rather focuses on a novel solution of easy content validation which was a big challenge in our project
	
## Getting Started
Clone the repository and enter the below in the terminal:
npm run cucu

### Prerequisites
Ensure that the below are installed:

	1. JDK 1.8+
	2. Node 8.0+
	3. Npm 4.0+
	4. chrome
	5. Visual studio code (IDE)
	6. Nvm (for changing the node version)
	7. Git

### Installing
Clone the package into your local machine and run the below command
```bash
npm istall
```
### Running a Test
To run the basic test for page content validation, run the below script
```bash
npm run cucu
```
### Adding Scripts in Package.json
The cmd line accepts the below arguments of which the ones marked with * are mandatory:
	
	market*
	product
	env*
	browser*
	tags
	
It is imperative to mention the below paths as well:
	
	feature file path
	step definition path
	cucumber html report generated

refer to 'cucu' script under scripts in package.json. The posttest script (postcucu) ususally runs after the test execution to generate the html report.

### Adding a new test
Test steps are added in standard cucumber format and the test steps are mapped to the functions in step-definitions. Step definitions create objects of pages to interact with the browser.

### Data Handling Stratergy:
This Framework relies on data mainted in cucumber feature file and the js file that maintains the content as an object.The content that needs to be validated is added to the content file with file name:
	
	<product-name>.content
	
Ensure that the above product name is same as the product name in config file. Configuration details like the urls and domain are maintained as a part of the configuration. Selector file under configuration maintains the all the element selectors

### on-Boarding a new market
The Framework is designed in a way that new markets could be added without any major code changes. To add a new Market, follow the below steps:

S1: Add configuration files under the specific market name under configuration

S2: Add the content files under the specific market name under content

S3: Copy the feature file from sample and add the market and product as parameters in the feature file. (this is to decouple the test cases from other markets)





	

