const Header = {
    cardname: 'The American Express Platinum Card',
    cardart: 'http://icm.aexp-static.com/Internet/IntlAcquisition/DECA/Intl-Digital-Acquisition/DYNA/static/au/803/platinumcharge/cardart/platinumcharge_small.gif'
};

const Sidebar = {
    cardname: 'THE AMERICAN EXPRESS PLATINUM CARD',
    cardart: 'http://icm.aexp-static.com/Internet/IntlAcquisition/DECA/Intl-Digital-Acquisition/DYNA/static/au/803/platinumcharge/cardart/platinumcharge_small.gif',
    phone:'1 300 557 743',
    faq:'https://icm.aexp-static.com/Internet/IntlAcquisition/DECA/Intl-Digital-Acquisition/DYNA/en_AU/common/faq/faq.html'
};

const aboutyou = {
    topsectionheader: 'Please provide a few basic details and we will review in as little as 30 seconds.',
    topsectionpara: 'To help process your application smoothly, please ensure your name matches what is on your identification.',
    bottomsectionheader: {
        1: 'Please make sure the information above is correct. We will use your personal information to:',
        2: 'For more details about how we collect, use and manage personal information, see our Card Member Privacy Statement.'
    },
    bottomsectionli: {
        1: 'Perform a credit check to determine your eligibility for this card. For more details see our Credit Reporting Information Policy.',
        2: 'Verify your identity and financial information with various third-party sources (such as government bodies/agencies, your employer, payroll/superannuation providers, financial institutions and data aggregators)',
        3: 'Contact you about this application and send you information about offers, products and services. You can change your marketing preferences at any time either online or by clicking \'unsubscribe\' within our marketing emails.'
    }
};

module.exports = {
    Header,Sidebar,aboutyou
};
