const Header = {
    cardname: 'The Qantas American Express Ultimate Card',
    cardart: 'http://icm.aexp-static.com/Internet/IntlAcquisition/DECA/Intl-Digital-Acquisition/DYNA/static/au/572/qantasultimate/cardart/qantasultimate_small.gif'
};

const Sidebar = {
    cardname: 'THE QANTAS AMERICAN EXPRESS ULTIMATE CARD',
    cardart: 'http://icm.aexp-static.com/Internet/IntlAcquisition/DECA/Intl-Digital-Acquisition/DYNA/static/au/572/qantasultimate/cardart/qantasultimate_small.gif'
};

const aboutyou = {
    topsectionheader: 'Please provide a few basic details and we will review in as little as 30 seconds.',
    topsectionpara: 'To help process your application smoothly, please ensure your name matches what is on your identification.',
    bottomsectionheader: {
        1: 'We will use and disclose your personal information to:',
        2: 'For more details about how we collect, use and manage personal information, see our Card Member Privacy Statement.'
    },
    bottomsectionli: {
        1: 'Perform a credit check - see our Credit Reporting Information Policy.',
        2: 'Verify your identity and financial information with third-party sources such as government agencies, financial institutions, data aggregators and our service provider Verifier. You agree that Verifier will, on your behalf, request access to data about your superannuation contributions, payroll and employment. Verifier will use that data to calculate your income and provide that to us.',
        3: 'Contact you, including to send you information about offers, products and services. You can change your marketing preferences either online or by clicking \'unsubscribe\' within our marketing emails.'
    }
};

module.exports = {
    Header,Sidebar,aboutyou
};